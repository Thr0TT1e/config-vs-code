let
  inherit (builtins) readFile;
in
{
  name.long = "Thr0TT1e";
  name.short = "ThreeT";
  username = "threet";
  email = "Thr0TT1e@gmail.com";
  #openpgp.id = "0x9254D45940949194";
  #openpgp.asc = ./andrew.asc;
  phone = "+7-915-417-00-33";
  #ssh = readFile ./andrew.pub;
  #image = ./andrew.jpg;
}

