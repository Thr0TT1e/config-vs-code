# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true;
  };

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Moscow";

  # Select internationalisation properties.
  i18n.defaultLocale = "ru_RU.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "ru_RU.UTF-8";
    LC_IDENTIFICATION = "ru_RU.UTF-8";
    LC_MEASUREMENT = "ru_RU.UTF-8";
    LC_MONETARY = "ru_RU.UTF-8";
    LC_NAME = "ru_RU.UTF-8";
    LC_NUMERIC = "ru_RU.UTF-8";
    LC_PAPER = "ru_RU.UTF-8";
    LC_TELEPHONE = "ru_RU.UTF-8";
    LC_TIME = "ru_RU.UTF-8";
  };

  # Enable the X11 windowing system.
  # You can disable this if you're only using the Wayland session.
  services.xserver.enable = true;

  # Enable the KDE Plasma Desktop Environment.
  services.displayManager.sddm.enable = true;
  services.desktopManager.plasma6.enable = true;

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us, ru";
    variant = "";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  hardware.pulseaudio.enable = false;
  #security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    defaultUserShell = pkgs.zsh;
    users = {
      threet = {
        description = "ThreeT";
        extraGroups = [
          "networkmanager"
          "wheel"
        ];
        isNormalUser = true;
        packages = [
          pkgs.keepassxc
          pkgs.rclone
          pkgs.linphone
          pkgs.thunderbird
          pkgs.telegram-desktop
          pkgs.skypeforlinux
        ];
        shell = pkgs.zsh;
        useDefaultShell = true;
      };
    };
  };

  nixpkgs.config = {
    allowUnfree = true; # Allow unfree packages
    permittedInsecurePackages = [
      "yandex-browser"
    ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment = {
    systemPackages = [
      # Настройка ОС
      pkgs.zsh
      pkgs.zsh-powerlevel10k
      pkgs.neovim
      pkgs.vimPlugins.nvchad
      pkgs.tmux
      pkgs.wget
      pkgs.aria
      pkgs.eza
      pkgs.bat
      pkgs.direnv
      pkgs.unzip
      pkgs.nixpkgs-fmt
      # Разное
      pkgs.onlyoffice-bin
      pkgs.vlc
      pkgs.brave
      pkgs.librewolf-wayland
      pkgs.tor
      pkgs.tor-browser
      pkgs.gimp
      pkgs.blender
      # Разработка
      pkgs.vscodium
      pkgs.nodejs_18
      pkgs.fnm
      pkgs.dbeaver-bin
      pkgs.postman
      pkgs.go
    ];
  };

  fonts = {
    packages = [
      pkgs.fira-code
      pkgs.fira-code-nerdfont
    ];
  };

  programs = {
    firefox = {
      enable = true;
    };
    git = {
      config = {
        init = {
          defaultBranch = "main";
        };
        user = {
          name = "Thr0TT1e";
          email = "7hr0771e@gmail.com";
        };
      };
      enable = true;
      lfs = {
        enable = true;
        package = pkgs.git-lfs;
      };
      package = pkgs.gitFull;
    };
    neovim = {
      defaultEditor = true;
      enable = true;
    };
    zsh = {
      autosuggestions = {
        async = true;
        enable = true;
      };
      enable = true;
      enableCompletion = true;
      ohMyZsh = {
        cacheDir = "\$HOME/.cache/oh-my-zsh";
        enable = true;
        plugins = [
          "git"
          "node"
          "npm"
          "tmux"
          "docker-compose"
          "rust"
          "command-not-found"
          "colored-man-pages"
          "systemd"
        ];
        #theme = "powerlevel10k/powerlevel10k";
      };
      syntaxHighlighting = {
        enable = true;
      };
      promptInit = "source ${pkgs.zsh-powerlevel10k}/share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
    };
  };

  security = {
    rtkit = {
      enable = true;
    };
    sudo = {
      wheelNeedsPassword = false;
    };
  };


  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?

  # Автоматические обновления системы
  system.autoUpgrade = {
  enable = true;
  # flake = inputs.self.outPath;
  flags = [
    "--update-input"
    "nixpkgs"
    "--print-build-logs"
  ];
  dates = "02:00";
  randomizedDelaySec = "45min";
};

}
