source /usr/share/cachyos-fish-config/cachyos-config.fish

# overwrite greeting
# potentially disabling fastfetch
function fish_greeting
   # smth smth
end

abbr -a fish-reload-config 'source ~/.config/fish/**/*.fish'

alias i="sudo pacman -S --needed"
alias iu="sudo pacman -Syu --needed"
alias s="pacman -Ss"

#--------------------- Rclone -------------------------------------
alias sync_in_db="rclone sync --interactive /home/threet/Документы/keepass dropbox:KeePass"
alias sync_from_pc="rclone sync --interactive dropbox:KeePass /home/threet/Документы/keepass"
