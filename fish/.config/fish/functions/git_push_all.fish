function git_push_all
    # Проверяем, что передано сообщение для коммита
    if test (count $argv) -eq 0
        echo "Пожалуйста, укажите сообщение для коммита."
        return 1
    end

    # Добавляем все изменения в индекс
    git add .

    # Выполняем коммит с заданным сообщением
    git commit -m "$argv"

    # Получаем список всех удалённых репозиториев
    set remotes (git remote)

    # Пушим изменения во все удалённые репозитории
    for remote in $remotes
        echo "Отправка изменений в $remote..."
        git push $remote master
    end

    echo "Коммит и пуш завершены."
end
