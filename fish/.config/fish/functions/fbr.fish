function fbr
    set -l branches (git branch --all | grep -v HEAD)

    set -l branch (printf "%s\n" $branches | \
        fzf-tmux -d (math 2 + (count $branches)) +m)

    if test -n "$branch"
        git checkout (echo $branch | sed "s/.* //" | sed "s#remotes/[^/]*/##")
    end
end
